package prefixtree

//
// Lets clients register values under key-prefixes
// Then lookup keys
//
// The mapping could be ambiguous but we only try to detect this
// when clients try to lookup keys (and not when the mapping is defined)
//

import (
	"fmt"
	"strings"
)

// Tree ...
type Tree map[string]interface{}

// Lookup finds an element matching the given key
// Returns error if no match can be found
// XXX: Implementation is slow, will need to revisit when data gets bigger
func (t Tree) Lookup(sought string) (interface{}, error) {
	var result interface{}
	var resultLength = 0
	for k, v := range t {
		if strings.HasPrefix(sought, k) {
			if len(k) > resultLength {
				resultLength = len(k)
				result = v
			}
		}
	}
	if result == nil {
		return nil, fmt.Errorf("No match for %q", sought)
	}
	return result, nil
}
