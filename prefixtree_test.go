package prefixtree

import (
	"errors"
	"testing"
)

var mapping = map[string]interface{}{
	"aa": "1",
	"b":  "2",
	"bb": "3",
}

var lookuptests = []struct {
	in  string
	out interface{}
	err error
}{
	{"aa", "1", nil},
	{"aaa", "1", nil},
	{"ba", "2", nil},
	{"b", "2", nil},
	{"bb", "3", nil},
	{"c", nil, errors.New("No match for \"c\"")},
}

func TestPrefixLookup(t *testing.T) {
	tree := Tree(mapping)
	for i, lt := range lookuptests {
		found, err := tree.Lookup(lt.in)
		if lt.err != nil {
			switch {
			case err == nil:
				t.Errorf("%d. Expected error %q", i, lt.err)
			case lt.err.Error() != err.Error():
				t.Errorf("%d. lookup(%q) => err(%q), want err(%q)", i, lt.in, err, lt.err)
			}
		}
		if lt.out != found {
			t.Errorf("%d. lookup(%q) => %q, want %q", i, lt.in, found, lt.out)
		}
	}
}
